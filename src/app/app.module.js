"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var routes_1 = require("./routes");
var index_1 = require("./authentication/index");
var index_2 = require("./common/index");
var index_3 = require("./geolocation/index");
var index_4 = require("./communication/index");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            router_1.RouterModule.forRoot(routes_1.appRoutes)
        ],
        declarations: [
            app_component_1.AppComponent,
            index_2.HeroBannerComponent,
            index_3.LocatedComponent,
            index_3.LocateMeComponent,
            index_1.LoginComponent,
            index_2.MastheadComponent,
            index_4.UserMessageComponent,
            index_1.WelcomeComponent,
            index_2.SimpleFooterComponent
        ],
        providers: [
            index_1.AuthenticationService,
            index_3.GeolocationService,
            index_1.FBInitParamsService,
            index_4.LoggerService,
            index_4.PersistenceService,
            index_4.UserMessageService,
            index_1.UserService
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
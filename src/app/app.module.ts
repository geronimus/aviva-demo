import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { appRoutes } from './routes';

import {
  AuthenticationService,
  FBInitParamsService,
  LoginComponent,
  UserService,
  WelcomeComponent
} from './authentication/index';

import {
  HeroBannerComponent,
  MastheadComponent,
  SimpleFooterComponent
} from './common/index';

import {
  GeolocationService,
  LocatedComponent,
  LocateMeComponent
} from './geolocation/index';

import {
  LoggerService,
  PersistenceService,
  UserMessageComponent,
  UserMessageService
} from './communication/index';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    AppComponent,
    HeroBannerComponent,
    LocatedComponent,
    LocateMeComponent,
    LoginComponent,
    MastheadComponent,
    UserMessageComponent,
    WelcomeComponent,
    SimpleFooterComponent
  ],
  providers: [
    AuthenticationService,
    GeolocationService,
    FBInitParamsService,
    LoggerService,
    PersistenceService,
    UserMessageService,
    UserService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

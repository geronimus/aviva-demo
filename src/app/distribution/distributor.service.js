"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var DistributorService = (function () {
    function DistributorService() {
    }
    DistributorService.prototype.locateNearest = function (myLocation) {
        // Use slice(0) to return a copy, not the original array
        // TODO: Implement location filtering
        return DISTRIBUTORS.slice(0);
    };
    return DistributorService;
}());
DistributorService = __decorate([
    core_1.Injectable()
], DistributorService);
exports.DistributorService = DistributorService;
// Mock array of distributors:
// TODO: Implement this as an HTTP call to the back end (eg Distributor MDM)
var DISTRIBUTORS = [
    {
        name: 'RBC Insurance Agency Ltd (Meadowvale)',
        branchCode: 'B006421',
        email: 'info@rbcinsurance.com',
        telephone: '905-286-5099',
        addressLine1: '6880 Financial Drive',
        city: 'Mississauga',
        province: 'ON',
        postalCode: 'L5N 7Y5',
        location: {
            latitude: 43.6086536,
            longitude: -79.74708
        }
    }
];
//# sourceMappingURL=distributor.service.js.map
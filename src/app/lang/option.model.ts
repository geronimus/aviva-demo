// A Scala-style (almost-) monadic encapsulation for results representing zero
// or one value.

/**
 * The base type of the IOption interface, which represents a naive version
 * of the Option monad, which the occurence of zero or one values of type <T>.
 */
export interface IOption<T> {}

/**
 * The None type of the IOption interface, which represents a result of zero
 * occurrences of whatever type.
 */
export class None<T> implements IOption<T> {}

/**
 * The ISome<T> type of the IOption interface, which represents a result of
 * one occurrence of type <T>.
 */
export interface ISome<T> extends IOption<T> {
  value : T;
}

/**
 * A means of constructing a concrete member that implements the ISome<T>
 * interface, which represents a result of one occurrence of type <T>.
 */
export class Some<T> implements ISome<T> {

  public value : T;

  constructor(someValue : T) {
    this.value = someValue;
  }
}

/**
 * A type guard for the ISome<T> type, representing a single instance of
 * the given type T.
 */
export function isSome<T>(testValue : IOption<T>) : testValue is ISome<T> {
  return ( (<ISome<T>>testValue).value !== undefined );
} 

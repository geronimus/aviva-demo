// A Go-style result object implementation allowing a single type to contain
// multiple pieces of info about whether or not an operation was successful.
"use strict";
/**
 * @description
 * Represents the successful completion of an operation.
 */
var Success = (function () {
    function Success(resultValue, resultMessage) {
        if (resultMessage === void 0) { resultMessage = ''; }
        this.success = true;
        this.message = resultMessage;
        if (resultValue) {
            this.value = resultValue;
        }
    }
    return Success;
}());
exports.Success = Success;
/**
 * @description
 * Represents a failure to complete an operation or erroneous termination.
 */
var Failure = (function () {
    function Failure(failureMessage, failureError, recoveryValue) {
        if (failureMessage === void 0) { failureMessage = ''; }
        this.success = false;
        if (failureError) {
            this.error = failureError;
        }
        if (recoveryValue) {
            this.value = recoveryValue;
        }
    }
    return Failure;
}());
exports.Failure = Failure;
//# sourceMappingURL=result.model.js.map
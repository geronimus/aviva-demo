// A Go-style result object implementation allowing a single type to contain
// multiple pieces of info about whether or not an operation was successful.

/**
 * @description
 * An interface to represent the result of any operation, allowing a single
 * return value to provide multiple types of information. 
 */
export interface IResult<T> {
  // Indicates whether the operation succeeded or failed.
  success : boolean;
  // An optional message about the result. Should default to the empty
  // string, if none is provided.
  message : string;
  // The value of the result. Will be undefined if this result is an error.
  value? : T;
  // An error object that may be returned if an operation completes in an error state.
  error? : Error;
}

/**
 * @description
 * Represents the successful completion of an operation.
 */
export class Success<T> implements IResult<T> {
  public success : boolean;
  public message : string;
  public value? : T;

  constructor(resultValue? : T, resultMessage = '' ) {
    this.success = true;
    this.message = resultMessage;

    if (resultValue) {
      this.value = resultValue;
    }
  }
}

/**
 * @description
 * Represents a failure to complete an operation or erroneous termination.
 */
export class Failure<T> implements IResult<T> {
  public success : boolean;
  public message : string;
  public error? : Error;
  public value? : T;

  constructor(failureMessage = '', failureError? : Error, recoveryValue? : T) {
    this.success = false;
    
    if (failureError) {
      this.error = failureError;
    }

    if (recoveryValue) {
      this.value = recoveryValue;
    }
  }
}
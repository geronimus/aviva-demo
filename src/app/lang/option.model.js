// A Scala-style (almost-) monadic encapsulation for results representing zero
// or one value.
"use strict";
/**
 * The None type of the IOption interface, which represents a result of zero
 * occurrences of whatever type.
 */
var None = (function () {
    function None() {
    }
    return None;
}());
exports.None = None;
/**
 * A means of constructing a concrete member that implements the ISome<T>
 * interface, which represents a result of one occurrence of type <T>.
 */
var Some = (function () {
    function Some(someValue) {
        this.value = someValue;
    }
    return Some;
}());
exports.Some = Some;
/**
 * A type guard for the ISome<T> type, representing a single instance of
 * the given type T.
 */
function isSome(testValue) {
    return (testValue.value !== undefined);
}
exports.isSome = isSome;
//# sourceMappingURL=option.model.js.map
import { Injectable } from '@angular/core';

import { IInitParams } from './index';

/**
 * @description
 * Holds the initialization parameters for this App to interact with the
 * Facebook SDK APIs.
 */
@Injectable()
export class FBInitParamsService {

  private params = {
    appId: '210357502826271',
    autoLogAppEvents: false,
    version: 'v2.8',
    xfbml: false
  };

  /**
   * @description
   * The public method returns a copy of the privately-defined initialization
   * parameters.
   */
  public getObject() : IInitParams { return Object.assign({}, this.params); }

}

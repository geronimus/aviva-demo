"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
// Importing this from a barrel causes an error:
var facebook_init_params_service_1 = require("./facebook-init-params.service");
var index_1 = require("../lang/index");
// The type the Facebook API provides is in the global scope,
// and cannot be encapsulated.
var FB = window['FB'];
var AuthenticationService = (function () {
    function AuthenticationService(initParams) {
        this.initParams = initParams;
    }
    AuthenticationService.prototype.getUser = function (callback) {
        this.fbInit()
            .then(this.getLoginStatus, function (reason) { console.log(reason); })
            .then(this.getLoggedInUser, function (reason) { console.log(reason); })
            .then(callback)
            .catch(function (reason) { console.log(reason); });
    };
    AuthenticationService.prototype.login = function (callback) {
        this.fbInit()
            .then(this.fbLogin, function (reason) { console.log(reason); })
            .then(this.getLoggedInUser, function (reason) { console.log(reason); })
            .then(callback)
            .catch(function (reason) { console.log(reason); });
    };
    AuthenticationService.prototype.logout = function (callback) {
        this.fbLogout()
            .then(callback)
            .catch(function (reason) { console.log(reason); });
    };
    AuthenticationService.prototype.fbInit = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                resolve(FB.init(_this.initParams.getObject()));
            }
            catch (e) {
                reject(e);
            }
        });
    };
    AuthenticationService.prototype.getLoginStatus = function () {
        return new Promise(function (resolve, reject) {
            try {
                FB.getLoginStatus(function (response) {
                    if (!response) {
                        reject('Could not get login status from Facebook API');
                    }
                    else {
                        resolve(response);
                    }
                });
            }
            catch (e) {
                reject(e);
            }
        });
    };
    AuthenticationService.prototype.fbLogin = function () {
        return new Promise(function (resolve, reject) {
            try {
                FB.login(function (response) {
                    if (!response) {
                        reject('Could not get login status from Facebook API');
                    }
                    else {
                        resolve(response);
                    }
                });
            }
            catch (e) {
                reject(e);
            }
        });
    };
    AuthenticationService.prototype.fbLogout = function () {
        return new Promise(function (resolve, reject) {
            try {
                FB.logout(function (response) { resolve(response); });
            }
            catch (e) {
                reject(e);
            }
        });
    };
    AuthenticationService.prototype.getLoggedInUser = function (loginStatus) {
        return new Promise(function (resolve, reject) {
            // If the status is not connected, return no signed in user
            if (loginStatus.status !== 'connected') {
                resolve(new index_1.None());
            }
            else {
                try {
                    FB.api('/me?fields=first_name,last_name', { feilds: ['first_name', 'last_name'] }, function (response) {
                        if (!response) {
                            reject('Could not get user details from Facebook API');
                        }
                        else {
                            resolve(new index_1.Some({
                                firstName: response.first_name,
                                lastName: response.last_name,
                                loginExpires: new Date(Date.now() + (loginStatus.authResponse.expiresIn * 1000)),
                                userId: loginStatus.authResponse.userID
                            }));
                        }
                    });
                }
                catch (e) {
                    resolve(new index_1.None());
                }
            }
        });
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [facebook_init_params_service_1.FBInitParamsService])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map
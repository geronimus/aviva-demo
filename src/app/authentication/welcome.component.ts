import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './index';

@Component({
  templateUrl: './welcome.component.html'
})
export class WelcomeComponent implements OnInit {

  public actionText : string;
  public infoText : string = 'Welcome to Aviva Canada';

  constructor(private router : Router, private user : UserService) {
    this.actionText = `${user.getFirstName()}`;
  }

  // Show the welcome message for a "moment", then navigate to the location
  // component
  ngOnInit() {
    this.user.checkValidity();
    setTimeout(
      () => { this.router.navigate(['locate']); },
      1500);
  }
}

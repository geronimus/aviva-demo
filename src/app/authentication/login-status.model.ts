import { IAuthResponse } from './auth-response.model';

export interface ILoginStatus {
  authResponse? : IAuthResponse;
  status : string;
}

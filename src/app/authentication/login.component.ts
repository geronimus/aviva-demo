import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './authentication.service';
import { PersistenceService } from '../communication/persistence.service';
import { UserService } from './user.service';
import { IOption, isSome } from '../lang/index';
import { IUser } from './index';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public actionText : string = 'Welcome';
  public infoText : string = 'to the Aviva Canada Customer Portal';

  constructor(
    private auth : AuthenticationService,
    private persist : PersistenceService,
    private router : Router,
    private user : UserService
  ) {}

  public login() : void {
    // After login, re-route to the same page to redo the authentication check,
    // and react as appropriate, depending on the result.;
    this.auth.login( this.authenticate );
  }

  ngOnInit() {
    this.auth.getUser( this.authenticate );
  }

  private authenticate = ( foundUser : IOption<IUser> ) => {
    if ( isSome<IUser>(foundUser) ) {
      if ( this.user.setUser(foundUser.value).success ) {
        this.persist.loginEvent(this.user);
        this.router.navigate(['welcome']);
      }
    }
  }
}

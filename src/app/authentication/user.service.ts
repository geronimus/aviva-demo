import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { IUser } from './index';
import { IPosition } from '../geolocation/index';
import { IResult, Success, Failure } from '../lang/index';

/**
 * @description
 * Provides information about the current user that can be called from
 * synchronous code.
 */
@Injectable()
export class UserService {

  private currentUser : IUser;

  constructor(private router : Router) {
    this.currentUser = null;
  }

  /**
   * @description
   * Getter for the current user's first name.
   */
  public getFirstName() : string {
    return this.getWithDefault('firstName', '');
  }

  /**
   * @description
   * Getter for the current user's first name.
   */
  public getLastName() : string {
    return this.getWithDefault('lastName', '');
  }

  /**
   * @description
   * Getter for the current user's login expiry date.
   */
  public getLoginExpires() : Date {
    return this.getWithDefault( 'loginExpires', new Date(0) );
  }

  /**
   * @description
   * Getter for the current user's user id.
   */
  public getUserId() : string {
    return this.getWithDefault('userId', 'none');
  }

  /**
   * @description
   * Determines whether the current user's log in is still valid.
   */
  public isValid() : boolean {
    if (
      this.isSet &&
      this.getLoginExpires() >= new Date( Date.now() )
    ) { return true; }
    else { return false; }
  }

  /**
   * @description
   * Determines whether or not the current user has been geolocated.
   */
  public isLocated() : boolean {
    return !!this.currentUser.location;
  }

  /**
   * @description
   * Allows the client to set geocoordinates for the user.
   * @param position 
   */
  public setLocation(position : IPosition) : void {
    this.currentUser.location = Object.assign({}, position);
  }

  /**
   * @description
   * Retrieves (a copy of) the geocoordinates of the user.
   */
  public getLocation() : IPosition {
    return Object.assign({}, this.currentUser.location);
  }

  /**
   * @description
   * Attempts to the current user to an IUser instance, and returns true if
   * the operation is successful. 
   * @param user An object that implements the IUser interface.
   */
  public setUser(user : IUser) : IResult<void> {
    try {
      this.currentUser = user;
      return new Success<void>();
    } catch (e) {
      return new Failure<void>('Unable to set the user', e);
    }
  }

  /**
   * @description
   * Removes the current user information, for example when the user logs out.
   */
  public unsetUser() : IResult<void> {
    try {
      if ( this.isSet ) {
        this.currentUser = null;
        return new Success<void>();
      }
      else {
        return new Failure<void>('User is not currently set');
      }
    } catch (e) {
      return new Failure<void>('Unable to unset user', e);
    }
  }

  /**
   * @description
   * Can be invoked when displaying new components to verify that the current
   * user is still valid. If not, immediately redirects to the login page.
   */
  public checkValidity() : void {
    try {
      if ( !this.isValid() ) { this.router.navigate(['login']); }
    } catch (e) {

    }
  }

  /**
   * @description
   * Determines whether the current user has been set.
   */
  private isSet() : boolean {
    let setState = this.currentUser ? true : false;
    return setState;
  }

  /**
   * Returns a user property if the current user is set, with the provided
   * default value if the current user is not set, or if the property is
   * unavailable.
   * 
   * (Note: If the default value and the user property are of different
   * types, the default value will be returned.)
   * 
   * @param propertyName The name of the user property to return.
   * @param defaultVal The value to return if the property cannot be retrieved.
   */
  private getWithDefault<T>(propertyName : string, defaultVal : T) : T {
    if ( this.isSet() && this.currentUser[propertyName] ) {
      try {
        return this.currentUser[propertyName];
      } catch (e) {
        return defaultVal;
      }
    }
    else {
      return defaultVal;
    }
  }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("./authentication.service");
var persistence_service_1 = require("../communication/persistence.service");
var user_service_1 = require("./user.service");
var index_1 = require("../lang/index");
var LoginComponent = (function () {
    function LoginComponent(auth, persist, router, user) {
        var _this = this;
        this.auth = auth;
        this.persist = persist;
        this.router = router;
        this.user = user;
        this.actionText = 'Welcome';
        this.infoText = 'to the Aviva Canada Customer Portal';
        this.authenticate = function (foundUser) {
            if (index_1.isSome(foundUser)) {
                if (_this.user.setUser(foundUser.value).success) {
                    _this.persist.loginEvent(_this.user);
                    _this.router.navigate(['welcome']);
                }
            }
        };
    }
    LoginComponent.prototype.login = function () {
        // After login, re-route to the same page to redo the authentication check,
        // and react as appropriate, depending on the result.;
        this.auth.login(this.authenticate);
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.auth.getUser(this.authenticate);
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        templateUrl: './login.component.html'
    }),
    __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
        persistence_service_1.PersistenceService,
        router_1.Router,
        user_service_1.UserService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map
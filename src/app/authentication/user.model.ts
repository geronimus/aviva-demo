import { IPosition } from '../geolocation/index';

export interface IUser {
  firstName : string;
  lastName : string;
  loginExpires : Date;
  userId : string;
  location? : IPosition;
}

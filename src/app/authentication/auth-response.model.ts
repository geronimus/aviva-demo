export interface IAuthResponse {
  accessToken : string;
  expiresIn : number;
  signedRequest : string;
  userID : string;
}

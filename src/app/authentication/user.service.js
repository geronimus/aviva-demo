"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("../lang/index");
/**
 * @description
 * Provides information about the current user that can be called from
 * synchronous code.
 */
var UserService = (function () {
    function UserService(router) {
        this.router = router;
        this.currentUser = null;
    }
    /**
     * @description
     * Getter for the current user's first name.
     */
    UserService.prototype.getFirstName = function () {
        return this.getWithDefault('firstName', '');
    };
    /**
     * @description
     * Getter for the current user's first name.
     */
    UserService.prototype.getLastName = function () {
        return this.getWithDefault('lastName', '');
    };
    /**
     * @description
     * Getter for the current user's login expiry date.
     */
    UserService.prototype.getLoginExpires = function () {
        return this.getWithDefault('loginExpires', new Date(0));
    };
    /**
     * @description
     * Getter for the current user's user id.
     */
    UserService.prototype.getUserId = function () {
        return this.getWithDefault('userId', 'none');
    };
    /**
     * @description
     * Determines whether the current user's log in is still valid.
     */
    UserService.prototype.isValid = function () {
        if (this.isSet &&
            this.getLoginExpires() >= new Date(Date.now())) {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * @description
     * Determines whether or not the current user has been geolocated.
     */
    UserService.prototype.isLocated = function () {
        return !!this.currentUser.location;
    };
    /**
     * @description
     * Allows the client to set geocoordinates for the user.
     * @param position
     */
    UserService.prototype.setLocation = function (position) {
        this.currentUser.location = Object.assign({}, position);
    };
    /**
     * @description
     * Retrieves (a copy of) the geocoordinates of the user.
     */
    UserService.prototype.getLocation = function () {
        return Object.assign({}, this.currentUser.location);
    };
    /**
     * @description
     * Attempts to the current user to an IUser instance, and returns true if
     * the operation is successful.
     * @param user An object that implements the IUser interface.
     */
    UserService.prototype.setUser = function (user) {
        try {
            this.currentUser = user;
            return new index_1.Success();
        }
        catch (e) {
            return new index_1.Failure('Unable to set the user', e);
        }
    };
    /**
     * @description
     * Removes the current user information, for example when the user logs out.
     */
    UserService.prototype.unsetUser = function () {
        try {
            if (this.isSet) {
                this.currentUser = null;
                return new index_1.Success();
            }
            else {
                return new index_1.Failure('User is not currently set');
            }
        }
        catch (e) {
            return new index_1.Failure('Unable to unset user', e);
        }
    };
    /**
     * @description
     * Can be invoked when displaying new components to verify that the current
     * user is still valid. If not, immediately redirects to the login page.
     */
    UserService.prototype.checkValidity = function () {
        try {
            if (!this.isValid()) {
                this.router.navigate(['login']);
            }
        }
        catch (e) {
        }
    };
    /**
     * @description
     * Determines whether the current user has been set.
     */
    UserService.prototype.isSet = function () {
        var setState = this.currentUser ? true : false;
        return setState;
    };
    /**
     * Returns a user property if the current user is set, with the provided
     * default value if the current user is not set, or if the property is
     * unavailable.
     *
     * (Note: If the default value and the user property are of different
     * types, the default value will be returned.)
     *
     * @param propertyName The name of the user property to return.
     * @param defaultVal The value to return if the property cannot be retrieved.
     */
    UserService.prototype.getWithDefault = function (propertyName, defaultVal) {
        if (this.isSet() && this.currentUser[propertyName]) {
            try {
                return this.currentUser[propertyName];
            }
            catch (e) {
                return defaultVal;
            }
        }
        else {
            return defaultVal;
        }
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [router_1.Router])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
/**
 * @description
 * Holds the initialization parameters for this App to interact with the
 * Facebook SDK APIs.
 */
var FBInitParamsService = (function () {
    function FBInitParamsService() {
        this.params = {
            appId: '210357502826271',
            autoLogAppEvents: false,
            version: 'v2.8',
            xfbml: false
        };
    }
    /**
     * @description
     * The public method returns a copy of the privately-defined initialization
     * parameters.
     */
    FBInitParamsService.prototype.getObject = function () { return Object.assign({}, this.params); };
    return FBInitParamsService;
}());
FBInitParamsService = __decorate([
    core_1.Injectable()
], FBInitParamsService);
exports.FBInitParamsService = FBInitParamsService;
//# sourceMappingURL=facebook-init-params.service.js.map
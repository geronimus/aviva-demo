import { Injectable } from '@angular/core';

import {
    ILoginStatus,
    IUser,
    IUserDetails
} from './index';
// Importing this from a barrel causes an error:
import { FBInitParamsService } from './facebook-init-params.service';
import { IOption, None, Some } from '../lang/index';

// The type the Facebook API provides is in the global scope,
// and cannot be encapsulated.
let FB : any = window['FB'];

@Injectable()
export class AuthenticationService {

  constructor( private initParams : FBInitParamsService ) {}
  
  public getUser( callback : (res : IOption<IUser>) => any ) {
    this.fbInit()
    .then( this.getLoginStatus, (reason) => { console.log(reason); } )
    .then( this.getLoggedInUser, (reason) => { console.log(reason); } )
    .then( callback )
    .catch( (reason) => { console.log(reason); } );
  }

  public login( callback : (res : IOption<IUser>) => any ) : void {
    this.fbInit()
    .then( this.fbLogin, (reason) => { console.log(reason); } )
    .then( this.getLoggedInUser, (reason) => { console.log(reason); } )
    .then( callback )
    .catch( (reason) => { console.log(reason); } );
  }

  public logout( callback : (res : any) => any ) : void {
    this.fbLogout()
    .then( callback )
    .catch( (reason) => { console.log(reason); } );
  }

  private fbInit() : Promise<any> {
    return new Promise(
      (resolve, reject) => {
        try {
          resolve( FB.init( this.initParams.getObject() ) );
        } catch (e) {
          reject(e);
        }
      }
    );
  }

  private getLoginStatus() : Promise<ILoginStatus> {
    return new Promise(
      (resolve, reject) => {
        try {
          FB.getLoginStatus(
            (response : ILoginStatus) => {
              if (!response) {
                reject('Could not get login status from Facebook API');
              }
              else {
                resolve(response);
              }
            }
          );
        } catch (e) {
          reject(e);
        }
      }
    );
  }

  private fbLogin() : Promise<ILoginStatus> {
    return new Promise(
      (resolve, reject) => {
        try {
          FB.login(
            (response : ILoginStatus) => {
              if (!response) {
                reject('Could not get login status from Facebook API');
              }
              else {
                resolve(response);
              }
            }
          );
        } catch (e) {
          reject(e);
        }
      }
    );
  }

  private fbLogout() : Promise<any> {
    return new Promise(
      (resolve, reject) => {
        try {
          FB.logout(
            (response : any) => { resolve(response); }
          );
        } catch (e) {
          reject(e);
        }
      }
    );
  }

  private getLoggedInUser(
    loginStatus : ILoginStatus
  ) : Promise<IOption<IUser>> {
    return new Promise(
      (resolve, reject) => {
        // If the status is not connected, return no signed in user
        if ( loginStatus.status !== 'connected' ) {
          resolve( new None<IUser>() );
        }
        // Otherwise, get the user details and construct an IUser object
        else {
          try {
            FB.api(
              '/me?fields=first_name,last_name',
              { feilds : [ 'first_name', 'last_name' ] },
              (response : IUserDetails) => {
                if (!response) {
                  reject('Could not get user details from Facebook API');
                }
                else {
                  resolve(
                    new Some<IUser>({
                      firstName: response.first_name,
                      lastName: response.last_name,
                      loginExpires: new Date(
                        Date.now() + (loginStatus.authResponse.expiresIn * 1000)
                      ),
                      userId: loginStatus.authResponse.userID
                    })
                  );
                }
              }
            );
          } catch (e) {
            resolve( new None<IUser>() );
          }
        }
      }
    );
  }
}

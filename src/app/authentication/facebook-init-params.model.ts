/**
 * This defines the type of the params object that the a call to
 * `FB.init(params)` (Facebook JavaScript SDK) expects.
 */
export interface IInitParams {
  /** The ID assigned to this app by the Facebook Developer process. */
  appId : string;
  /** Indicates whether app events are logged automatically. Defaults to false. */
  autoLogAppEvents : boolean;
  /** The Facebook SDK version to be used. Must be an available version. */
  version : string;
  /** Determines whether the XFBML tags are parsed and rendered. Defaults
   * to false.
   */
  xfbml : boolean;
}
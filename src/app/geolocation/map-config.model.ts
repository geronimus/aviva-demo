import { IPos } from './index';

export interface IMapConfig {
  center : IPos;
  zoom : number;
}

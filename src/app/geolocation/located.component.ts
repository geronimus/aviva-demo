import {
  AfterViewInit,
  ApplicationRef,
  Component,
  OnInit
} from '@angular/core';

import { GeolocationService } from './geolocation.service';
import { PersistenceService } from '../communication/persistence.service';
import { UserService } from '../authentication/user.service';

import { IMessage, UserMessageService } from '../communication/index';

/**
 * @description
 * This component exposes the business services available to a located user.
 */
@Component({
  templateUrl: './located.component.html'
})
export class LocatedComponent implements AfterViewInit, OnInit {

  public actionText : string;
  public infoText : string;

  public messageReady : boolean;
  public message : IMessage;

  constructor(
    private appRef : ApplicationRef,
    private geolocation : GeolocationService,
    private persist : PersistenceService,
    private user : UserService,
    private userMessages : UserMessageService
  ) {

    this.actionText = 'Tell us';
    this.infoText = 'what you need';

    this.messageReady = false;
    this.message = null;
  }

  public autoLossAlert() : void {
    this.persist.locateEvent(this.user, 'auto accident alert');

    // In a real application, we would get the delay from some kind of service,
    // but for the purposes of the example, we're hard-coding it.
    let delay = 7;
    this.showNewMessage( this.userMessages.claimsContact(delay) );
  }

  public homeLossAlert() : void {
    this.persist.locateEvent(this.user, 'property incident alert');

    let delay = 6;
    this.showNewMessage( this.userMessages.claimsContact(delay) );
  }

  public showNewMessage(messageObject : IMessage) : void {
    // Append the message to the component
    this.message = messageObject;

    // Set the display flag to true
    this.messageReady = true;

    // Refresh the view
    this.appRef.tick();
  }

  ngOnInit() {
    this.user.checkValidity();
    window['initMap'] = this.geolocation.initMap;
  }

  ngAfterViewInit() {
    // Add the script required to run the Google Maps Geolocation API
    if ( !document.getElementById('googleMapsAPI') ) {
      let bodyTag : HTMLElement = document.getElementsByTagName('body')[0];
      let googleScript : HTMLScriptElement = document.createElement('script');
      googleScript.id = 'googleMapsAPI';
      googleScript.async = true;
      googleScript.defer = true;
      googleScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDanTeUqPsOau8hV2uUgYUfFrfQnT_6zT8&callback=initMap';
      bodyTag.appendChild(googleScript);
    }
    else {
      this.geolocation.initMap();
    }
  }
}

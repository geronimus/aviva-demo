import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication/authentication.service';
import { UserService } from '../authentication/user.service';

@Component({
  templateUrl: './locate-me.component.html'
})
export class LocateMeComponent implements OnInit {

  public actionText : string;
  public infoText : string;  

  constructor(
    private auth : AuthenticationService,
    private router : Router,
    private user : UserService
  ) {
    this.actionText = 'Tell us';
    this.infoText = 'where you are';
  }

  public locateMe() : void {
    this.router.navigate(['local-services']);
  }

  ngOnInit() {
    this.user.checkValidity();
  }
}

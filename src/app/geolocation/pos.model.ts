/**
 * @description
 * The specific shape of a map point used by the Google Maps geolocation API.
 */
export interface IPos {
  lat : number;
  lng : number;
}

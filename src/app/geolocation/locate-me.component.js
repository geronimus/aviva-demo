"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("../authentication/authentication.service");
var user_service_1 = require("../authentication/user.service");
var LocateMeComponent = (function () {
    function LocateMeComponent(auth, router, user) {
        this.auth = auth;
        this.router = router;
        this.user = user;
        this.actionText = 'Tell us';
        this.infoText = 'where you are';
    }
    LocateMeComponent.prototype.locateMe = function () {
        this.router.navigate(['local-services']);
    };
    LocateMeComponent.prototype.ngOnInit = function () {
        this.user.checkValidity();
    };
    return LocateMeComponent;
}());
LocateMeComponent = __decorate([
    core_1.Component({
        templateUrl: './locate-me.component.html'
    }),
    __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
        router_1.Router,
        user_service_1.UserService])
], LocateMeComponent);
exports.LocateMeComponent = LocateMeComponent;
//# sourceMappingURL=locate-me.component.js.map
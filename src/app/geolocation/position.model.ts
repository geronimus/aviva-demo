/**
 * @description
 * The map point model used by the browser coords model, and by our service.
 */
export interface IPosition {
  latitude : number;
  longitude : number;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var geolocation_service_1 = require("./geolocation.service");
var persistence_service_1 = require("../communication/persistence.service");
var user_service_1 = require("../authentication/user.service");
var index_1 = require("../communication/index");
/**
 * @description
 * This component exposes the business services available to a located user.
 */
var LocatedComponent = (function () {
    function LocatedComponent(appRef, geolocation, persist, user, userMessages) {
        this.appRef = appRef;
        this.geolocation = geolocation;
        this.persist = persist;
        this.user = user;
        this.userMessages = userMessages;
        this.actionText = 'Tell us';
        this.infoText = 'what you need';
        this.messageReady = false;
        this.message = null;
    }
    LocatedComponent.prototype.autoLossAlert = function () {
        this.persist.locateEvent(this.user, 'auto accident alert');
        // In a real application, we would get the delay from some kind of service,
        // but for the purposes of the example, we're hard-coding it.
        var delay = 7;
        this.showNewMessage(this.userMessages.claimsContact(delay));
    };
    LocatedComponent.prototype.homeLossAlert = function () {
        this.persist.locateEvent(this.user, 'property incident alert');
        var delay = 6;
        this.showNewMessage(this.userMessages.claimsContact(delay));
    };
    LocatedComponent.prototype.showNewMessage = function (messageObject) {
        // Append the message to the component
        this.message = messageObject;
        // Set the display flag to true
        this.messageReady = true;
        // Refresh the view
        this.appRef.tick();
    };
    LocatedComponent.prototype.ngOnInit = function () {
        this.user.checkValidity();
        window['initMap'] = this.geolocation.initMap;
    };
    LocatedComponent.prototype.ngAfterViewInit = function () {
        // Add the script required to run the Google Maps Geolocation API
        if (!document.getElementById('googleMapsAPI')) {
            var bodyTag = document.getElementsByTagName('body')[0];
            var googleScript = document.createElement('script');
            googleScript.id = 'googleMapsAPI';
            googleScript.async = true;
            googleScript.defer = true;
            googleScript.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDanTeUqPsOau8hV2uUgYUfFrfQnT_6zT8&callback=initMap';
            bodyTag.appendChild(googleScript);
        }
        else {
            this.geolocation.initMap();
        }
    };
    return LocatedComponent;
}());
LocatedComponent = __decorate([
    core_1.Component({
        templateUrl: './located.component.html'
    }),
    __metadata("design:paramtypes", [core_1.ApplicationRef,
        geolocation_service_1.GeolocationService,
        persistence_service_1.PersistenceService,
        user_service_1.UserService,
        index_1.UserMessageService])
], LocatedComponent);
exports.LocatedComponent = LocatedComponent;
//# sourceMappingURL=located.component.js.map
import { Injectable, ApplicationRef } from '@angular/core';

import { IMapConfig, IPos } from './index';

import { PersistenceService } from '../communication/persistence.service';
import { UserService } from '../authentication/user.service';

// The objects used by the Google Maps API must be accessible in the global
// scope, and their types cannot be encapsulated.
let google : any;
let map : any;
let infoWindow : any;

// Wrap the global 'Navigator' object.
let navigator : any = window['navigator'];

// Because the google API appears to explicitly set its `this` reference to
// the window object, a proxy reference to the service will be needed.
let me : GeolocationService;

@Injectable()
export class GeolocationService {

  // The centre of the country is approximately at the east side of Sand Lake
  // Provincial Park (Manitoba).
  private defaultMap : IMapConfig = {
    center: { lat: 57.563268, lng: -97.2669257 },
    zoom: 3
  };

  constructor(
    private appRef : ApplicationRef,
    private user : UserService,
    private persist : PersistenceService
  ) { me = this; }

  initMap() : void {
    // Instantiate the main objects used by the geolocation service.
    google = window['google'];
    map = new google.maps.Map( me.mapElement(), me.defaultMap );
    infoWindow = new google.maps.InfoWindow;

    // Attempt HTML5 geolocation feature
    if ( navigator.geolocation ) {
      navigator.geolocation.getCurrentPosition(
        (position : any) => {
          let pos : IPos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          // Add the geocoordinates to the user object:
          me.user.setLocation( { latitude: pos.lat, longitude: pos.lng } );

          // Log the locate event:
          me.persist.locateEvent(me.user, 'locate');

          infoWindow.setPosition(pos);
          infoWindow.setContent('We found you here');
          infoWindow.open(map);
          map.setCenter(pos);
          map.setZoom(14);
          me.appRef.tick();
        },
        () => {
          me.handleLocationError(
            true,
            infoWindow,
            map.getCenter()
          );
        }
      );
    }
    else {
      // The browser doesn't support geolocation
      me.handleLocationError(false, infoWindow, map.getCenter() );
    }
  }

  /**
   * @description
   * Follows Google's documented example for how to gracefully handle a
   * geolocation error.
   * @param browserHasGeolocation The result of testing for the geolocation
   * feature in the browser, eg ( navigator.geolocation )
   * @param infoWindow The InfoWindow object provided by the Google Maps
   * geolocation API, and added to this class, as a convenience. 
   * @param pos A position object provided by the Google Maps API, and
   * returned by some of its methods, eg map.getCenter()
   */
  handleLocationError(
    browserHasGeolocation : boolean,
    myInfoWindow : any,
    pos : IPos
  ) : void {
    myInfoWindow.setPosition(pos);
    myInfoWindow.setContent(
      browserHasGeolocation
      ? 'The geolocation service is not available right now.'
      : 'The browser you\'re using doesn\'t support geolocation.'
    );
    myInfoWindow.open(map);
  }

  // Returns the page element whose id attribute is "map"
  private mapElement() : HTMLElement {
    return document.getElementById('map');
  };
}

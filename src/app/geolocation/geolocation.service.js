"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var persistence_service_1 = require("../communication/persistence.service");
var user_service_1 = require("../authentication/user.service");
// The objects used by the Google Maps API must be accessible in the global
// scope, and their types cannot be encapsulated.
var google;
var map;
var infoWindow;
// Wrap the global 'Navigator' object.
var navigator = window['navigator'];
// Because the google API appears to explicitly set its `this` reference to
// the window object, a proxy reference to the service will be needed.
var me;
var GeolocationService = (function () {
    function GeolocationService(appRef, user, persist) {
        this.appRef = appRef;
        this.user = user;
        this.persist = persist;
        // The centre of the country is approximately at the east side of Sand Lake
        // Provincial Park (Manitoba).
        this.defaultMap = {
            center: { lat: 57.563268, lng: -97.2669257 },
            zoom: 3
        };
        me = this;
    }
    GeolocationService.prototype.initMap = function () {
        // Instantiate the main objects used by the geolocation service.
        google = window['google'];
        map = new google.maps.Map(me.mapElement(), me.defaultMap);
        infoWindow = new google.maps.InfoWindow;
        // Attempt HTML5 geolocation feature
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                // Add the geocoordinates to the user object:
                me.user.setLocation({ latitude: pos.lat, longitude: pos.lng });
                // Log the locate event:
                me.persist.locateEvent(me.user, 'locate');
                infoWindow.setPosition(pos);
                infoWindow.setContent('We found you here');
                infoWindow.open(map);
                map.setCenter(pos);
                map.setZoom(14);
                me.appRef.tick();
            }, function () {
                me.handleLocationError(true, infoWindow, map.getCenter());
            });
        }
        else {
            // The browser doesn't support geolocation
            me.handleLocationError(false, infoWindow, map.getCenter());
        }
    };
    /**
     * @description
     * Follows Google's documented example for how to gracefully handle a
     * geolocation error.
     * @param browserHasGeolocation The result of testing for the geolocation
     * feature in the browser, eg ( navigator.geolocation )
     * @param infoWindow The InfoWindow object provided by the Google Maps
     * geolocation API, and added to this class, as a convenience.
     * @param pos A position object provided by the Google Maps API, and
     * returned by some of its methods, eg map.getCenter()
     */
    GeolocationService.prototype.handleLocationError = function (browserHasGeolocation, myInfoWindow, pos) {
        myInfoWindow.setPosition(pos);
        myInfoWindow.setContent(browserHasGeolocation
            ? 'The geolocation service is not available right now.'
            : 'The browser you\'re using doesn\'t support geolocation.');
        myInfoWindow.open(map);
    };
    // Returns the page element whose id attribute is "map"
    GeolocationService.prototype.mapElement = function () {
        return document.getElementById('map');
    };
    ;
    return GeolocationService;
}());
GeolocationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [core_1.ApplicationRef,
        user_service_1.UserService,
        persistence_service_1.PersistenceService])
], GeolocationService);
exports.GeolocationService = GeolocationService;
//# sourceMappingURL=geolocation.service.js.map
"use strict";
var index_1 = require("./authentication/index");
var index_2 = require("./geolocation/index");
exports.appRoutes = [
    { path: 'login', component: index_1.LoginComponent },
    { path: 'welcome', component: index_1.WelcomeComponent },
    { path: 'locate', component: index_2.LocateMeComponent },
    { path: 'local-services', component: index_2.LocatedComponent },
    { path: '', redirectTo: '/login', pathMatch: 'full' }
];
//# sourceMappingURL=routes.js.map
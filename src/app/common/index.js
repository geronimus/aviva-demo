"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require("./hero-banner.component"));
__export(require("./masthead.component"));
__export(require("./simple-footer.component"));
//# sourceMappingURL=index.js.map
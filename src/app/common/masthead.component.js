"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("../authentication/authentication.service");
var user_service_1 = require("../authentication/user.service");
var persistence_service_1 = require("../communication/persistence.service");
var MastheadComponent = (function () {
    function MastheadComponent(auth, router, persist, user) {
        this.auth = auth;
        this.router = router;
        this.persist = persist;
        this.user = user;
    }
    /**
     * @description
     * (Asynchronously) logs out the user using the Facebook authentication service,
     * and if successful, returns the app to the login screen.
     */
    MastheadComponent.prototype.logout = function () {
        var _this = this;
        this.auth.logout(function (resp) {
            _this.persist.logoutEvent(_this.user);
            _this.user.unsetUser();
            if (window.innerWidth < 768) {
                document.getElementById('navbarMenu').click();
            }
            _this.router.navigate(['/login']);
        });
    };
    return MastheadComponent;
}());
MastheadComponent = __decorate([
    core_1.Component({
        selector: 'masthead',
        templateUrl: './masthead.component.html'
    }),
    __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
        router_1.Router,
        persistence_service_1.PersistenceService,
        user_service_1.UserService])
], MastheadComponent);
exports.MastheadComponent = MastheadComponent;
//# sourceMappingURL=masthead.component.js.map
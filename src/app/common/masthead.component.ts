import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication/authentication.service';
import { UserService } from '../authentication/user.service';
import { PersistenceService } from '../communication/persistence.service';

@Component({
  selector: 'masthead',
  templateUrl: './masthead.component.html'
})
export class MastheadComponent {

  constructor(
    private auth : AuthenticationService,
    private router : Router,
    private persist : PersistenceService,
    private user : UserService
  ) {}

  /**
   * @description
   * (Asynchronously) logs out the user using the Facebook authentication service,
   * and if successful, returns the app to the login screen.
   */
  public logout() : void {
    this.auth.logout(
      (resp : any) => {
        this.persist.logoutEvent(this.user);
        this.user.unsetUser();
        if (window.innerWidth < 768) {
          document.getElementById('navbarMenu').click();
        }
        this.router.navigate(['/login']);
      }
    );
  }
}

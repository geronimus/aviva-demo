import { Component, Input } from '@angular/core';

@Component({
  selector: 'hero-banner',
  templateUrl: './hero-banner.component.html'
})
export class HeroBannerComponent {
  @Input() actionText : string = '';
  @Input() infoText : string = '';
}

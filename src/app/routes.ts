import { Routes } from '@angular/router';

import {
  LoginComponent,
  WelcomeComponent
} from './authentication/index';

import {
  LocatedComponent,
  LocateMeComponent
} from './geolocation/index';

export const appRoutes : Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'locate', component: LocateMeComponent },
  { path: 'local-services', component: LocatedComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

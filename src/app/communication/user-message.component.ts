import { Component, Input } from '@angular/core';

@Component({
  selector: 'user-message',
  templateUrl: './user-message.component.html'
})
export class UserMessageComponent {
  @Input()
  public heading : string = '';
  @Input()
  public message : string = '';
}

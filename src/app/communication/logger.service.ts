import { Injectable } from '@angular/core';

/**
 * @description
 * Currently, just a pointless hero-banner around console.log. But eventually,
 * a service for sending browser events back to the server.
 */
@Injectable()
export class LoggerService {
  stdout(output : any) : void { console.log(output); }
  stderr(output : any) : void { console.log(output); }
}

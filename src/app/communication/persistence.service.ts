import { Injectable } from '@angular/core';

import { ILocateEvent, ILoginEvent } from './index';
import { LogoutEvent } from './event.model';

import { UserService } from '../authentication/user.service';
import { LoggerService } from './logger.service';

/**
 * @description
 * A somewhat silly object-relational mapper around the app's events,
 * facilitating their eventual persistence to a database, or transmission
 * via a messaging service.
 */
@Injectable()
export class PersistenceService {

  constructor(private log : LoggerService, private user : UserService) {}

  public loginEvent(user : UserService) : void {
    let event : ILoginEvent = {
      eventTime: new Date(),
      eventName: 'authenticate',
      userId: user.getUserId(),
      userFirstName: user.getFirstName(),
      userLastName: user.getLastName(),
      userLoginExpires: user.getLoginExpires()
    };

    this.log.stdout(event);
  }

  public logoutEvent(user : UserService) : void {
    let event : LogoutEvent = new LogoutEvent(user);

    this.log.stdout(event);
  }

  public locateEvent(user : UserService, whichEvent : string) : void {
    let event : ILocateEvent = {
      eventTime: new Date(),
      eventName: whichEvent,
      userId: user.getUserId(),
      userFirstName: user.getFirstName(),
      userLastName: user.getLastName(),
      location: user.getLocation()
    };

    this.log.stdout(event);
  }
}

import { Injectable } from '@angular/core';

import { IMessage } from './index';

/**
 * @description
 * A provider of dynamic text for messages displayed to the user.
 */
@Injectable()
export class UserMessageService {

  // In production, the message would be fetched from a database, but for
  // the purposes of this demo, we will hard-code it.
  public claimsContact(delayInMinutes : number) : IMessage {
    return {
      heading: 'Help is on the way',
      message: `Our claims care team will contact you by phone in about ${delayInMinutes} minutes`
    };
  }
}


"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var event_model_1 = require("./event.model");
var user_service_1 = require("../authentication/user.service");
var logger_service_1 = require("./logger.service");
/**
 * @description
 * A somewhat silly object-relational mapper around the app's events,
 * facilitating their eventual persistence to a database, or transmission
 * via a messaging service.
 */
var PersistenceService = (function () {
    function PersistenceService(log, user) {
        this.log = log;
        this.user = user;
    }
    PersistenceService.prototype.loginEvent = function (user) {
        var event = {
            eventTime: new Date(),
            eventName: 'authenticate',
            userId: user.getUserId(),
            userFirstName: user.getFirstName(),
            userLastName: user.getLastName(),
            userLoginExpires: user.getLoginExpires()
        };
        this.log.stdout(event);
    };
    PersistenceService.prototype.logoutEvent = function (user) {
        var event = new event_model_1.LogoutEvent(user);
        this.log.stdout(event);
    };
    PersistenceService.prototype.locateEvent = function (user, whichEvent) {
        var event = {
            eventTime: new Date(),
            eventName: whichEvent,
            userId: user.getUserId(),
            userFirstName: user.getFirstName(),
            userLastName: user.getLastName(),
            location: user.getLocation()
        };
        this.log.stdout(event);
    };
    return PersistenceService;
}());
PersistenceService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [logger_service_1.LoggerService, user_service_1.UserService])
], PersistenceService);
exports.PersistenceService = PersistenceService;
//# sourceMappingURL=persistence.service.js.map
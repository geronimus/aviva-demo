"use strict";
var LogoutEvent = (function () {
    function LogoutEvent(user) {
        this.eventName = 'logout';
        this.eventTime = new Date();
        this.userId = user.getUserId();
        this.userFirstName = user.getFirstName();
        this.userLastName = user.getLastName();
    }
    return LogoutEvent;
}());
exports.LogoutEvent = LogoutEvent;
//# sourceMappingURL=event.model.js.map
import { IPosition } from '../geolocation/index';
import { UserService } from '../authentication/user.service';

export interface IEvent {
  eventTime : Date;
  eventName : string;
  userId : string;
  userFirstName : string;
  userLastName : string;
}

export interface ILoginEvent extends IEvent {
  userLoginExpires : Date;
}

export class LogoutEvent implements IEvent {

  public eventTime : Date;
  public eventName = 'logout';
  public userId : string;
  public userFirstName : string;
  public userLastName : string;

  constructor(user : UserService) {
    this.eventTime = new Date();
    this.userId = user.getUserId();
    this.userFirstName = user.getFirstName();
    this.userLastName = user.getLastName();
  }
}

export interface ILocateEvent extends IEvent {
  location : IPosition;
}

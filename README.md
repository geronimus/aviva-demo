#Aviva Customer Portal Demo

This project was created as a proof-of-concept for various functionalities that it may be possible to incorporate into an insurer's customer portal.

It is intended as an exploratory visualization only, and is not indicative of any actual system under development.

The trademarks used are for the purposes of the demonstration only and remain the property of their owners.

##To run
Simply:

  1. Clone the project and change to its top-level directory
  2. Install node dependencies: `npm install`
  3. Run with: `npm start`
